import pandas as pd
from random import randint, choices, sample
from test import sim_liste

prob = pd.read_csv("./Results/prob.csv", sep=";", header=0)
items = pd.read_csv("./Results/items.csv", sep=";", header=None)

nbrFoods = 6
drinks = [items.iloc[i, 1] for i in range(nbrFoods)]
foods = [items.iloc[i, 1] for i in range(nbrFoods, len(items))]


def food_generator(time):
    weight = [prob.iloc[nbrFoods + time, j] for j in range(nbrFoods, len(items))]
    selection = choices(foods, weights=weight, k=1)
    return selection


def drink_generator(time):
    weight = [prob.iloc[time, j] for j in range(1, nbrFoods + 1)]
    selection = choices(drinks, weights=weight, k=1)
    return selection


withdrawnREG = []
withdrawnTAD = []


def customer_generator(once, trip):
    gen_liste = sim_liste
    customer = choices(["Once", "Returning"], weights=[once, 1-once], k=1)
    print(customer)
    if customer[0] == "Returning":
        index = randint(0, len(gen_liste))
        return gen_liste[index]
    elif customer[0] == "Once":
        one = choices(["Regular", "TripAdvisor"], weights=[1-trip, trip], k=1)
        if one[0] == "Regular":
            index = randint(10000000, 99999999)
            while index in withdrawnREG:
                index = randint(10000000, 99999999)
            withdrawnREG.append(index)
            rdm = "ONE" + str(index)
            return rdm
        elif one[0] == "TripAdvisor":
            index = randint(10000000, 99999999)
            while index in withdrawnTAD:
                index = randint(10000000, 99999999)
            withdrawnTAD.append(index)
            rdm = "TAD" + str(index)
            return rdm


who = [customer_generator(0.5, 0.1) for i in range(20)]
print(who)
