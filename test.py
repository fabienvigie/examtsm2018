"""
Name : Simulation
Authors : Fabien Vigié, Adriana De La Cruz

The Simulation script aims to simulate the life of a coffee bar over a certain amount of time. It generate the time,
the customers and what he/she purchased.

The script is organised as follows :
The first part generates a pool of returning customers while that second part defines functions that will be passed
as argument of Parallel() (to speed up code execution). What concerns part three, it is just a way to define a range
of price through a dictionary. Then, part four focuses on the simulation itself and uses
what have been produced in part one and two to carry out the simulation. Finally, part five is about some analytics
such as displaying customer habits or plotting.

"""
###########################################################
# PART 3
###########################################################

###########################################################
#   1. Loading libraries and scripts
###########################################################
from myClass import *
import pandas as pd
import datetime
import numpy as np
from joblib import Parallel, delayed
import multiprocessing
import time
import os
import operator
from random import randint, choices, sample
import matplotlib.pyplot as plt
import matplotlib
matplotlib.style.use('ggplot')

time_begin = time.time()  # Just a way to measure how long it takes to run the code

###########################################################
#   2. Parameters
###########################################################

# Parameters for part 3
nbrReturning = 10  # Defines the total amount of returning customers
shrHipster = 1/3     # Defines the share of hipsters among the returning customers

# Parameters for part 6
nbrDrinks = 6          # Tells python how many food item are to be loaded. DO NOT CHANGE UNLESS ADDING A NEW FOOD ITEM!
probOneTime = 0      # Probabilities to get a one time customer
probTripAdvisor = 0.1  # Probabilities to get a TripAdvisor customer

# Parameter simulation
date_begin = "01-01-2013"   # Beginning date for simulation
date_end = "28-02-2013"     # End date for simulation

# Inflation :
inflation = True
inflation_begin = "01-06-2013"
increase = 1.2

###########################################################
#   3. Data loading
###########################################################

"""
In this section we just simply load different outputs from Exploratory.py such as the different "times" or "item"
sold by the coffee bar. Notice that we also load the "prob" matrix that contains the different probabilities to
choose one item at a given time.
"""
#   3.1. The matrix that contains the probabilities (probabilities to choose a cookie at 08:00...)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
prob = pd.read_csv("./Results/prob.csv", sep=";", header=0)

#   3.2. Load the different "time" where customers arrive (08:00, 08:05...)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
hours = pd.read_csv("./Results/hours.csv", sep=';', header=None)

#   3.3. Load the different items sold (notice that nbrDrinks is very import here)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
items = pd.read_csv("./Results/items.csv", sep=";", header=None)
drinks = [items.iloc[i, 1] for i in range(nbrDrinks)]   # The list of drinks sold
foods = [items.iloc[i, 1] for i in range(nbrDrinks, len(items))]  # The list of food item sold


###########################################################
#   4. Building pool of returning customers
###########################################################

# *********************************************************
#   4.1. Definition of variables (that do not depend from user-parameters defined in section 2.)
# *********************************************************

#   4.1.1. Defining the amount of hipsters among the returning customers that is just the total amount of returning
# customers times the share of hipsters
nbrHipster = int(nbrReturning * shrHipster)

#   4.1.2. Defining the complementary amount of regular customers
nbrRegular = nbrReturning - nbrHipster

# *********************************************************
#   4.2 Drawing a pool of returning customers
# *********************************************************
"""
A customer number is made of two components : a 3-letter part (RET for RETurning, HIP for HIPsters) and 
a 8-number part that is unique.
"""
sample = sample(range(10000000, 99999999), k=nbrReturning)  # Generate the unique 8-digit part
listingRET = ["RET" + str(sample[i]) for i in range(nbrRegular)]  # Then we concatenate a fraction to "RET"
listingHIP = ["HIP" + str(sample[i]) for i in range(nbrRegular, nbrReturning)]  # The same for "HIP"
listingRET = [Returning(listingRET[i]) for i in range(len(listingRET))]  # We create the customer accounts for RET
listingHIP = [Hipster(listingHIP[i]) for i in range(len(listingHIP))]   # Idem for HIPsters
listings = listingRET + listingHIP  # And merge the two lists into a single one

#############################################################
#   5. Pricing
#############################################################

"""
Two dictionaries containing for each item its corresponding price  
"""
my_foods = {"nothing": 0, "sandwich": 5, "cookie": 2, "muffin": 3, "pie": 3}
my_drinks = {"water": 2, "milkshake": 5, "frappucino": 4, "soda": 3, "coffee": 3, "tea": 3}

most_expensive_food = max([i for i in my_foods.values()])
most_expensive_drink = max([i for i in my_drinks.values()])

print(most_expensive_drink, most_expensive_food)
