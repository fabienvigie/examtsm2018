"""
Name : Exploratory
Authors : Fabien Vigié, Adriana De La Cruz
OS : Linux Ubuntu 14.04 LTS (works also fine on Macs)

Explanatory.py is a script aimed at finding and analysing a .csv file called Coffeebar_2013-2017.csv

Contents;
1.1. Library imports
1.2. Loading data set
1.3. Determine probabilities
    1.3.1. Questions
    1.3.2. Plotting
    1.3.3. Average
1.4. Additional computations (to be used in simulation)
1.5. Recap

"""
###########################################################
# PART 1
###########################################################

###########################################################
#   1.1. Library imports
###########################################################


import pandas as pd
import os
import numpy as np
import time
from joblib import Parallel, delayed
import multiprocessing
import matplotlib.pyplot as plt


###########################################################
#   1.2. Loading data set
###########################################################

begin = time.time()
#   1.2.1. Defining the location
path = os.path.abspath('./Data/Coffeebar_2013-2017.csv')  # initialising the location
print(path)  # prints out the absolute path (the one on your computer)

#   1.2.2. Loading the data set
my_data = pd.read_csv(path, sep=';', header=0)

#   1.2.3. Turn my_data into a data frame
my_data = pd.DataFrame(my_data)

#   1.2.4. Treating NaN
my_data["FOOD"] = my_data["FOOD"].fillna("nothing")  # If there is a missing value in the column "FOOD"
#  then the NaN is overwritten by "Nothing".

###########################################################
#   1.3. Determine probabilities
###########################################################

# *********************************************************
#   1.3.1. Questions :
# *********************************************************

#   1.3.1.1 What food and drinks are sold by the coffee bar?
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

drinks = my_data["DRINKS"].unique()  # Stores the different drinks proposed
drinks = np.array(drinks).tolist()  # convert drinks from an array to a list
print("The coffee bar has the following drinks :" + str(drinks))

# We have to find which type of food is sold (and get rid of "Nothing")
# Obtaining all different types of food on the SUBSET that does NOT contain "Nothing" in column "FOOD"
foods = my_data[my_data["FOOD"] != "nothing"]["FOOD"].unique()
foods = np.array(foods).tolist()  # convert foods from an array to a list
print("The coffee bar proposes the following food :" + str(foods))


#   1.3.1.2. How many unique customers did the bar have?
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

unique_customers = my_data["CUSTOMER"].unique()  # Create a list of all customers ever got in the bar
nbrCustomer = len(unique_customers)  # The number of unique customers is the size of the list of customers
print("The coffee bar had " + str(nbrCustomer) + " unique customers over its life!")

stop = 10   # Amount of second to stop the code running. Allows you to have the time to read above output
print("You have " + str(stop) + " seconds to read the above output!")
time.sleep(stop)


# *********************************************************
#   1.3.2. Plotting
# *********************************************************

#   1.3.2.1. Total amount of food sold
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# For each item in the list "foods", add to the list the amount sold, the amount sold corresponds to the size
# of my_data in which the column "FOOD" only takes the value "food"...
amount_food = [len(my_data[my_data["FOOD"] == food]) for food in foods]

# Plotting
plt.bar(foods, amount_food)
plt.xlabel("Food Type")
plt.ylabel("Quantity")
plt.title("Amount of food sold over 5 years.")
plt.grid(True)
plt.savefig(os.path.abspath('./Results/food_sold.png'))
plt.clf()

#   1.3.2.2. Total amount of drinks sold
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# For each item in the list "drinks", add to the list the amount sold, the amount sold corresponds to the size
# of my_data in which the column "DRINKS" only takes the value "drink"...
amount_drink = [len(my_data[my_data["DRINKS"] == drink]) for drink in drinks]

# Plotting
plt.bar(drinks, amount_drink)
plt.xlabel('Drink Type')
plt.ylabel('Quantity')
plt.title('Total amount of drinks sold over 5 years.')
plt.grid(True)
plt.savefig(os.path.abspath('./Results/drink_sold.png'))
plt.clf()


# *********************************************************
#   1.3.3. Average
# *********************************************************

"""
In this section, we want to compute the probability that a customer chooses a drink or a food given the time.
However, doing so implies to make several loops that might take some time (depending of your computer).
In order to speed up looping, we use parallelisation.
This section is split in three sub-parts : the first one is made only of functions that would be "parallelised",
the second part consists of making calculations using the different cores of CPU. Finally, the last part write
the outputs into a data frame whose rows are the different times and columns the different drinks and foods. 
"""

#   1.3.3.1. Defining functions
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#   Extracting time from column 'TIME'
def my_time(i):
    print(my_data["TIME"][i][11:16])
    return my_data["TIME"][i][11:16]


#   Compute the probability to choose a certain drink at a given time
def average_drink(heures, boissons):
    daily_consumption = len(my_data[my_data["HOURS"] == heures])
    data_temp = my_data[(my_data["HOURS"] == heures) & (my_data["DRINKS"] == boissons)]
    print(heures, boissons, (len(data_temp) / daily_consumption) * 100)
    return (len(data_temp) / daily_consumption) * 100


#   Compute the probability to choose a certain food at a given time
def average_food(heures, aliment):
    daily_consumption = len(my_data[my_data["HOURS"] == heures])
    data_temp = my_data[(my_data["HOURS"] == heures) & (my_data["FOOD"] == aliment)]
    print(heures, aliment, (len(data_temp) / daily_consumption) * 100)
    return (len(data_temp) / daily_consumption) * 100


#   1.3.3.2. Computations through parallelisation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#   1.3.3.2.1. Time extraction from column "TIME"
num_cores = multiprocessing.cpu_count()  # We get the number of CPU cores on the machine
print("Number of cores used:" + str(num_cores))  # Just a message telling the amount of cores available
my_data["HOURS"] = Parallel(n_jobs=num_cores)(delayed(my_time)(i) for i in range(len(my_data)))
# transitory receives the different times in a day. Results got from parallelisation
print("Time extraction done!")  # A little message that the operation is done

#   1.3.3.2.2. Getting unique values of time and storage in a newly created column
hours = my_data["HOURS"].unique()  # We store in a variable called "hours" the different values of time
print("Creation of HOURS done!")

#   1.3.3.2.3. Creation of variable analogous to "foods" but this one includes "nothing"
nothingFoods = my_data["FOOD"].unique()
nothingFoods = np.array(nothingFoods).tolist()

#   1.3.3.2.4. Computation of probabilities through parallelisation
result_drink = Parallel(n_jobs=num_cores)(delayed(average_drink)(heures, boissons)
                                          for heures in hours for boissons in drinks)
result_food = Parallel(n_jobs=num_cores)(delayed(average_food)(heures, aliment)
                                         for heures in hours for aliment in nothingFoods)
print("Probability computations done!")


#   1.3.3.3. Writing output in a data frame
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#   1.3.3.3.1. Creation of an empty data frame
items = drinks + nothingFoods  # Concatenation of drinks and nothingFoods
prob = pd.DataFrame(index=hours, columns=items)  # the empty data frame

#   1.3.3.3.2. Writing outputs in the empty data frame
j = -1
size_drink = len(drinks)
shift2 = -size_drink

for i in range(len(hours)):
    shift2 = shift2 + size_drink
    j += 1
    for k in range(size_drink):
        prob.iloc[j, k] = result_drink[shift2+k]

size_food = len(nothingFoods)
shift2 = -size_food
j = -1
for i in range(len(hours)):
    shift2 += size_food
    j += 1
    for k in range(size_food):
        prob.iloc[j, size_drink + k] = result_food[shift2 + k]
print("Write down in data frame done!")

#   1.3.3.3.2. Writing prob in a .csv file
outpath = os.path.abspath('./Results/prob.csv')
prob.to_csv(outpath, sep=';')
print("Write down to .csv done!")

#   1.3.3.3.3. Writing hours in .csv for later use
hours = pd.DataFrame(hours)
hours.to_csv("./Results/hours.csv", sep=";", header=None)

#   1.3.3.3.4. Writing the different items
items = pd.DataFrame(items)
items.to_csv("./Results/items.csv", sep=";", header=None)

###########################################################
# 1.4. Additional computations (to be used in simulation)
###########################################################

# *********************************************************
# 1.4.1. The prices that have to be applied
# *********************************************************

my_foods = {"nothing": 0, "sandwich": 5, "cookie": 2, "muffin": 3, "pie": 3}
my_drinks = {"water": 2, "milkshake": 5, "frappucino": 4, "soda": 3, "coffee": 3, "tea": 3}


# *********************************************************
# 1.4.2. Defining a function that computes profits
# *********************************************************

def my_profits(index):
    profit_drinks = my_drinks[my_data.iloc[index, 2]]  # Profits from selling a drink
    profit_foods = my_foods[my_data.iloc[index, 3]]    # Profits from selling food
    total_profits = profit_drinks + profit_foods       # Adding up both
    print(total_profits)
    return total_profits                               # Return the sum

# *********************************************************
# 1.4.3. Hard computations (with parallel())
# *********************************************************


original_profits = Parallel(n_jobs=num_cores)(delayed(my_profits)(index) for index in range(len(my_data)))

# *********************************************************
# 1.4.4. Building the hourly profit vector
# *********************************************************
"""
Instead of big explanations, here is an example : 
Suppose for TWO DAYS that, hours = [8:00, 9:00] and 
profits = [10, 5, 15, 14] = [10 (at 8am day 1), 5 (at 9am day 1), 15 (at 8am day 2)...] 
What the thereunder loop does : hourly_profits = [10 + 15, 5+14] = [25, 19]
"""
original_hourly_profits = []  # Empty list of hourly_profits
for i in range(len(hours)):
    hour_profit = 0
    for j in range(0, len(original_profits), len(hours)):
        hour_profit += original_profits[i+j]
    original_hourly_profits.append(hour_profit/1826)  # I acknowledge that I could have chosen a better way than
    print(hour_profit/1826)                           # writing 1826 directly (number of days over 5 years)

# *********************************************************
# 1.4.5. Transformation to data frame and writing outputs
# *********************************************************
original_hourly_profits = pd.DataFrame(original_hourly_profits)
original_hourly_profits.to_csv("./Results/original_hourly_profits.csv", sep=";", header=None)


###########################################################
# 1.5. Recap
###########################################################
print("The coffee bar has the following drinks :" + str(drinks))
print("The coffee bar proposes the following food :" + str(foods))
print("The coffee bar had " + str(nbrCustomer) + " unique customers over its life!")
print("Quantities of food sold :", amount_food)
print("Quantities of drinks sold :", amount_drink)

# TIME ####################################################
total_time = time.time() - begin - stop
minutes = int(total_time / 60)
seconds = int(((total_time / 60) - minutes) * 60)
print("Simulation carried out in {} minutes and {} seconds!".format(str(minutes), str(seconds)))
###########################################################
