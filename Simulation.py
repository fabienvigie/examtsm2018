"""
Name : Simulation
Authors : Fabien Vigié, Adriana De La Cruz
OS : Linux Ubuntu 14.04 LTS (works also fine on Macs)

The Simulation script aims to simulate the life of a coffee bar over a certain amount of time. It generates the time,
the customers and what he/she purchased.

Contents;
Loading libraries and scripts
3. Part 3
    3.1. Simulation parameters
    3.2. Data loading
    3.3. Building pool of returning customers
    3.4. Pricing
    3.5. Functions (to be parallelised)
        3.5.1. Generate which aliment will be consumed (if any) given the probabilities at a given time
        3.5.2. Writing "date", "time" and "full_hour"
    3.6. Simulation
        3.6.1. Generating dates
        3.6.2. Getting a few more elements
        3.6.3. Data generation
        3.6.4. Customer generation
        3.6.5. Writing the simulation to .csv
4. Part 4
    4.1 Comparisons
        4.1.1. Profits
        4.1.2. Compare Quantity of food and drinks sold
    4.2. Building the lists of returning customers (intermediary calculations)
    4.3. Show some buying histories of returning customers
        4.3.1. Draw a Regular customer history
        4.3.2. Draw a Hipster customer history
    4.4. How many returning customers?
    4.5. Do they have specific times when they show up more?
        4.5.1. For hipsters
        4.5.2. For Regular returning customers
        4.5.3. Both of them
    4.6. Can you determine a probability of having a onetime or returning customer at a given time?
    4.7. How does this impact their buying history?
    4.8. Recap

"""


###########################################################
#   Loading libraries and scripts
###########################################################
from myClass import *
import pandas as pd
import datetime
import numpy as np
from joblib import Parallel, delayed
import multiprocessing
import time
import os
from random import randint, choices, sample
import matplotlib.pyplot as plt


time_begin = time.time()  # Just a way to measure how long it takes to run the code

###########################################################
# PART 3
###########################################################

###########################################################
#   3.1. Simulation parameters
###########################################################

# Parameters for generating the pool of returning customers
nbrReturning = 1000  # Defines the total amount of returning customers
shrHipster = 1/3     # Defines the share of hipsters among the returning customers

# Parameters for customer generation
nbrDrinks = 6          # Tells python how many food item are to be loaded. DO NOT CHANGE UNLESS ADDING A NEW FOOD ITEM!
probOneTime = 0.8      # Probabilities to get a one time customer
probTripAdvisor = 0.1  # Probabilities to get a TripAdvisor customer

# Parameter simulation dates
date_begin = "01-01-2013"   # Beginning date for simulation
date_end = "31-12-2017"     # End date for simulation

# Inflation :
inflation = False               # Enables to inflate price, "True" => prices will be inflated
inflation_begin = "01-01-2015"  # Beginning date of inflation
increase = 1.2                  # Size of inflation

###########################################################
#   3.2. Data loading
###########################################################

"""
In this section we just simply load different outputs from Exploratory.py such as the different "times" or "item"
sold by the coffee bar. Notice that we also load the "prob" matrix that contains the different probabilities to
choose one item at a given time.
"""
#   3.2.1. The matrix that contains the probabilities (probabilities to choose a cookie at 08:00...)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
prob = pd.read_csv("./Results/prob.csv", sep=";", header=0)

#   3.2.2. Load the different "time" where customers arrive (08:00, 08:05...)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
hours = pd.read_csv("./Results/hours.csv", sep=';', header=None)

#   3.2.3. Load the different items sold (notice that nbrDrinks is very import here)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
items = pd.read_csv("./Results/items.csv", sep=";", header=None)
drinks = [items.iloc[i, 1] for i in range(nbrDrinks)]   # The list of drinks sold
foods = [items.iloc[i, 1] for i in range(nbrDrinks, len(items))]  # The list of food item sold


###########################################################
#   3.3. Building pool of returning customers
###########################################################

# *********************************************************
#   3.3.1. Definition of variables (that do not depend from user-parameters defined in section 2.)
# *********************************************************

#   3.3.1.1. Defining the amount of hipsters among the returning customers that is just the total amount of returning
# customers times the share of hipsters
nbrHipster = int(nbrReturning * shrHipster)

#   3.3.1.2. Defining the complementary amount of regular customers
nbrRegular = nbrReturning - nbrHipster

# *********************************************************
#   3.3.2. Drawing a pool of returning customers
# *********************************************************
"""
A customer number is made of two components : a 3-letter part (RET for RETurning, HIP for HIPsters) and 
a 8-number part that is unique.
"""
sample = sample(range(10000000, 99999999), k=nbrReturning)  # Generate the unique 8-digit part
listingRET = ["RET" + str(sample[i]) for i in range(nbrRegular)]  # Then we concatenate a fraction to "RET"
listingHIP = ["HIP" + str(sample[i]) for i in range(nbrRegular, nbrReturning)]  # The same for "HIP"
listingRET = [Returning(listingRET[i]) for i in range(len(listingRET))]  # We create the customer accounts for RET
listingHIP = [Hipster(listingHIP[i]) for i in range(len(listingHIP))]   # Idem for HIPsters
listings = listingRET + listingHIP  # And merge the two lists into a single one

#############################################################
#   3.4. Pricing
#############################################################

"""
Two dictionaries containing for each item its corresponding price  
"""
# *********************************************************
# 3.4.1. Dictionaries of item sold
# *********************************************************

my_foods = {"nothing": 0, "sandwich": 5, "cookie": 2, "muffin": 3, "pie": 3}
my_drinks = {"water": 2, "milkshake": 5, "frappucino": 4, "soda": 3, "coffee": 3, "tea": 3}

# *********************************************************
# 3.4.2. Most expensive items
# *********************************************************

"""
If a customer cannot purchase the most expensive item, then he/she would be automatically withdrawn from
the list of returning customers ("listings").
"""
most_expensive_food = max([i for i in my_foods.values()])
most_expensive_drink = max([i for i in my_drinks.values()])

#############################################################
#   3.5. Functions (to be parallelised)
#############################################################

"""
Here starts the tricky part. In order to carry out the simulation, we generate column by column the "date", "time",
"food", "drink" and "Full_hour" (just the hour-part of a given time). This is why there are 5 different 
functions to carry out computations for each column. Notice that for update issue, the customer generation is a whole
script more below (not a function, explains why more below as well).
Notice also that we use functions so they can be passed as arguments of the Parallel() function to speed up the
process.
*food_generator, drink_generator : The idea of these functions is to read from prob data frame the probabilities to 
choose a given aliment and draw one random aliment that would be eaten (or drank).
*date_generator, time_generator : just return an element from a list. These two functions only exist so they can be
passed as argument of Parallel()
*full_hour gives the hour-part of the given time on the same row. For example, if the time on row i is "08:45", then
full_hour gives "08" (will be used later on).
"""

# *********************************************************
#   3.5.1. Generate which aliment will be consumed (if any) given the probabilities at a given time
# *********************************************************

"""
Ideas of how these two functions work :
They load the probabilities from prob matrix written at the end of Explanatory.py. 
Now, for the sake of simplicity, suppose that you have this [A, B, C] with respective probabilities [0.5, 0.4, 0.1]
We just pick a A, B or C according to these probabilities and return the picked up element.
"""


def food_generator(index):
    weight = [prob.iloc[index, nbrDrinks + 1 + j] for j in range(len(items)-nbrDrinks)]
    selection = choices(foods, weights=weight, k=1)
    print(index, selection[0])
    return selection[0]
# "item" and "foods" are defined in part 3 (data loading) whereas "nbrDrink" is defined in part 2
# Example : weight = 20%, 10%, 10%, 30%, 30% (for each aliment proposed) => pick up the corresponding aliment randomly


def drink_generator(index):
    weight = [prob.iloc[index, j] for j in range(1, nbrDrinks + 1)]
    selection = choices(drinks, weights=weight, k=1)
    print(index, selection[0])
    return selection[0]
# "drinks" and "food" are defined in part 3 (data loading) whereas "nbrDrink" is defined in part 2

# *********************************************************
#   3.5.2. Writing "date", "time" and "full_hour"
# *********************************************************


# Just a way to speed up the copy past of dates and time using parallel()
def date_generator(index):
    print(index, mydate.iloc[index, 0])
    return mydate.iloc[index, 0]


def time_generator(index):
    print(index, hours.iloc[index, 1])
    return hours.iloc[index, 1]


def full_hour(index):
    print(my_simulation.iloc[index, 1][0:2])
    return my_simulation.iloc[index, 1][0:2]

###########################################################
#   3.6. Simulation
###########################################################

# *********************************************************
#   3.6.1. Generating dates
# *********************************************************


start = datetime.datetime.strptime(date_begin, "%d-%m-%Y")  # begin date
end = datetime.datetime.strptime(date_end, "%d-%m-%Y")  # end date
step = datetime.timedelta(days=1)  # step in days
length = (end-start).days  # total length in days
mydate = []  # the empty list that is going to receive all the generated dates
while start <= end:  # We append a new date to my_date
    mydate.append(start.date())
    start += step
mydate = pd.DataFrame(mydate)  # turn it into a data frame


# *********************************************************
#   3.6.2. Getting a few more elements
# *********************************************************

# 3.6.2.1. Counting the number of permutations possible
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

permut = len(hours) * len(mydate)  # length of hours times length of my_date
print("There are {} permutations possible".format(permut))

# 3.6.2.2. Getting the amount of threads on the CPU
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

num_cores = multiprocessing.cpu_count()  # We get the number of CPU cores on the machine

# 3.6.2.3. Creating an empty data frame that we are going to fill up
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

my_simulation = pd.DataFrame(data=None, index=range(permut), columns=["Date", "Time", "Customer", "Drink", "Food",
                                                                      "Full_Hour"])

# *********************************************************
# 3.6.3. Data generation
# *********************************************************

# Each colum is generated through parallelisation (customers exepted).

my_simulation["Date"] = Parallel(n_jobs=num_cores)(delayed(date_generator)(i)
                                                   for i in range(len(mydate)) for j in range(len(hours)))
my_simulation["Time"] = Parallel(n_jobs=num_cores)(delayed(time_generator)(k)
                                                   for i in range(len(mydate)) for k in range(len(hours)))
my_simulation["Drink"] = Parallel(n_jobs=num_cores)(delayed(drink_generator)(i)
                                                    for m in range(len(mydate)) for i in range(len(hours)))
my_simulation["Food"] = Parallel(n_jobs=num_cores)(delayed(food_generator)(i)
                                                   for n in range(len(mydate)) for i in range(len(hours)))
my_simulation["Full_Hour"] = Parallel(n_jobs=num_cores)(delayed(full_hour)(index) for index in range(len(my_simulation))
                                                        )

# *********************************************************
# 3.6.4. Customer generation
# *********************************************************
"""
    We do not generate customer through parallelisation because of update reasons. In order to understand what this 
means, we should understand how parallel() works. When parallel() is called, the function parallelised can pick up 
what it needs everywhere in the script (date, time, food, drink...) and even write outputs somewhere (for example, store
a value in an empty list). But as soon as the function in parallel stops, everything that had been written will 
disappear excepted what the function returns. 
    So when we pick up a returning customer, we make him/her purchase the coffee bar items. The history is updated and
the customer budget is decreased. But if that customer was about to be re-withdrawn from the pool, he/she will be
a "new" customer with a full initial budget and an empty history. 
    The only way to sort out this problem is to put the updated object into the pool of returning customer (at the same 
place). But, the parallel() function does not allow this... 
"""
# 3.6.4.1. Customer generation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

regular_counter = 9999999  # A counter that count the amount of regular customer, used as 8-digit part of ID
hipster_counter = 9999999  # Same thing with hipsters
output = []                # An empty list that will contains customer accounts and profits from customer
# Designed this way in order to consider tips paid in profits


for index, i in enumerate(my_simulation["Customer"]):
    # 1. Initialisation #########
    #   1.1. Get some data (day, time, which aliment...)
    date = my_simulation.iloc[index, 0]
    temps = my_simulation.iloc[index, 1]
    drink = my_simulation.iloc[index, 3]
    food = my_simulation.iloc[index, 4]

    #   1.2. Is there inflation expected?
    # If inflation enabled and current date above inflation begin date,
    if inflation is True and date >= datetime.datetime.strptime(inflation_begin, "%d-%m-%Y").date():
        # Prices are increased by the amount of inflation
        food_price = increase*my_foods[food]  # Storing the inflated food price
        drink_price = increase*my_drinks[drink]     # Storing the inflated drink price
        inflated = increase     # Used to inflate maximal prices when checking if enough budget
    else:
        food_price = my_foods[food]     # Storing the normal food price
        drink_price = my_drinks[drink]  # Storing the normal drink price
        inflated = 1  # If no inflation "inflated" = 1

    #   1.3. Draw a type of customer (returning, one-time)
    if len(listings) == 0:  # If all returning cutomers are unable to pay (meaning the pool is empty)
        customer = ["Once"]  # We generate inly "One-time" cutomers
    else:
        customer = choices(["Once", "Returning"], weights=[probOneTime, 1 - probOneTime], k=1)
    #   2. Execution #########
    #   Treating two cases : one-time and returning
    #   2.1. Returning customer
    if customer[0] == "Returning":
        rank = randint(0, len(listings)-1)  # draw a random number
        client = listings[rank]    # This random number corresponds to an index in the "listing" list ...
        # ... of returning customers
        client.payh(date=date, time=temps, drink=drink, dprice=drink_price, food=food, fprice=food_price)
        listings[rank] = client
        print(index, client.id)
        # because "listings" contains objects of class Returning, we can apply methods attached to this class
        # so the returning customer is buying some drink (and food) at a given date and time
        # notice that we keep a record of this transaction through the PAY with History method.
        output.append([client, food_price + drink_price])  # Returns the client account and the profit made
        if client.budget < inflated * (most_expensive_food + most_expensive_drink):
            # If the customer has not enough budget to purchase items, he/she is taken away from the pool
            listings.remove(client)

    #   2.2. OneTime customer
    elif customer[0] == "Once":
        # We chose randomly a regular or TripAdvisor customer with a probability given by the user
        one = choices(["Regular", "TripAdvisor"], weights=[1-probTripAdvisor, probTripAdvisor], k=1)
        # Two cases : Regular and TripAdvisor customers
        #   2.2.1. Regular one-time customer
        if one[0] == "Regular":
            regular_counter += 1
            number = regular_counter
            client = "ONE" + str(number)    # concatenate with "ONE" that number
            client = OneTime(client)    # creating a customer account
            client.pay(dprice=drink_price, fprice=food_price)
            print(index, client.id)
            # make this customer pay (notice we use the pay method only => no record need)
            output.append([client, food_price + drink_price])  # return the customer account and the profit made
        #   2.2.2. TripAdvisor customer (same reasoning than "regular" customer)
        elif one[0] == "TripAdvisor":
            hipster_counter += 1
            number = hipster_counter
            client = "TAD" + str(number)
            client = TripAdvisor(client)
            client.pay(dprice=drink_price, fprice=food_price)
            print(index, client.id)
            tip = client.pay_tip()  # but TripAdvisor customers can pay tips
            output.append([client, food_price + drink_price + tip])

# 3.6.4.2. Filling "Customer" column
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Because output is a list made of lists (customer accounts and profits), we have to pick up the customer id only
# to store it in the column "customer" (to get the customer id, we apply the method .id)
my_simulation["Customer"] = [output[i][0].id for i in range(len(my_simulation["Customer"]))]


# *********************************************************
# 3.6.5. Writing the simulation to .csv
# *********************************************************

my_simulation.to_csv("./Results/my_simulation.csv", sep=";", header=None)

###########################################################
# PART 4 (including end of Part 3 in exam)
###########################################################

###########################################################
#   4.1 Comparisons
###########################################################
"""
The aim of section 4.1 is to create plots so we can compare the profits and amount of item sold between
the original data and the simulation.
"""

# *********************************************************
# 4.1.1. Profits
# *********************************************************

# 4.1.1.1. For the simulation part
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
profits = [output[i][1] for i in range(len(my_simulation["Customer"]))]  # Extraction of profits from output
# We recall that "output" is a list of lists in which profits are located on the second position in inner lists
hourly_profit = []  # Building an empty list that will contain profits per hours

"""
Instead of big explanations, here is an example : 
Suppose for TWO DAYS that, hours = [8:00, 9:00] and 
profits = [10, 5, 15, 14] = [10 (at 8am day 1), 5 (at 9am day 1), 15 (at 8am day 2)...] 
What the thereunder loop does : hourly_profits = [10 + 15, 5+14] = [25, 19]
"""
for i in range(len(hours)):
    hour_profit = 0
    for j in range(0, len(profits), len(hours)):
        hour_profit += profits[i+j]
    hourly_profit.append(hour_profit / len(mydate))

# 4.1.1.2. For the original Coffee bar file
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# We just load data that we got from running Explanatory.py
original_hourly_profits = pd.read_csv("./Results/original_hourly_profits.csv", sep=";", header=None)
original_hourly_profits = original_hourly_profits[1]  # In column [0], there are indexes => profits are on col [1]


# 4.1.1.3. Plotting the two series
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

plt.plot(hours[1], hourly_profit, color='b')  # We first plot the simulation hourly_profits
plt.plot(hours[1], original_hourly_profits, color='r')  # Then we add on the same plot the original hourly_profits
plt.xlabel("Time")
plt.ylabel("Average Profits")
plt.title("Hourly profits over 5 years.")
plt.savefig(os.path.abspath('./Results/hourly_profits.png'))
plt.clf()

# note : essayer de présenter les heures un peu mieux sur le graphique

# *********************************************************
# 4.1.2. Compare Quantity of food and drinks sold
# *********************************************************

# 4.1.2.1. Generate the vectors containing the item sold
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Get the item sold by the coffee bar by subsetting "my_simulation" and applying the method unique()
no_nothing_foods = my_simulation[my_simulation["Food"] != "nothing"]["Food"].unique()
no_nothing_foods = np.array(no_nothing_foods).tolist()  # Conversion to a list

# 4.1.2.2. Get the amount of each item sold over the simulation
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Again, we subset "my_simulation" where the relevant item is sold and then count using len() function
amount_food = [len(my_simulation[my_simulation["Food"] == food]) for food in no_nothing_foods]
amount_drink = [len(my_simulation[my_simulation["Drink"] == drink]) for drink in drinks]

# 4.1.2.3. Plotting item sold
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Plotting for food item sold
plt.bar(no_nothing_foods, amount_food)
plt.xlabel("Food Type")
plt.ylabel("Quantity")
plt.title("Amount of food sold over 5 years (simulation).")
plt.grid(True)
plt.savefig(os.path.abspath('./Results/food_sold_simulation.png'))
plt.clf()

# Plotting for drinks sold
plt.bar(drinks, amount_drink)
plt.xlabel('Drink Type')
plt.ylabel('Quantity')
plt.title('Total amount of drinks sold over 5 years (simulation).')
plt.grid(True)
plt.savefig(os.path.abspath('./Results/drink_sold_simulation.png'))
plt.clf()

# Print the amount of each item sold
print("Total quantity of food sold :", amount_food)
print("Total quantity of drinks sold :", amount_drink)

###########################################################
# 4.2. Building the lists of returning customers (intermediary calculations)
###########################################################

# 4.2.1. Creating two lists that will contains the different returning customer accounts
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

returning_regular = []  # Contains all regular returning customers
returning_hipster = []  # Contains all hipster returning customers

# 4.2.2. Put each returning customer in the right list
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

for position in range(len(output)):                     # For all customers in the output list
    if output[position][0].id[0:3] == "RET":            # If the id of customer starts by "RET" :
        returning_regular.append(output[position][0])   # We add the customer account in the Regular list
    elif output[position][0].id[0:3] == "HIP":          # Otherwise, if the id starts by "HIP" :
        returning_hipster.append(output[position][0])   # Add the customer account to Hipster list


# 4.2.3. Turn lists to data frame to get only once each returning customer
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

returning_regular = pd.DataFrame(returning_regular)
returning_hipster = pd.DataFrame(returning_hipster)

# 4.2.4. Get each returning customer (only once in the list)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

returning_regular = returning_regular[0].unique()
returning_hipster = returning_hipster[0].unique()

###########################################################
# 4.3. Show some buying histories of returning customers
###########################################################
"""
In section 4.3. We show buying histories of two customers : one regular, and one hipster
"""

# *********************************************************
# 4.3.1. Draw a Regular customer history
# *********************************************************

random_regular = randint(0, len(returning_regular)-1)  # We draw a random number that will be used as index
regular_selected = returning_regular[random_regular]   # This random number represents an index in the list
regular_selected.records()                             # We get the related customer account and print its history

# *********************************************************
# 4.3.2. Draw a Hipster customer history
# *********************************************************

random_hipster = randint(0, len(returning_hipster)-1)  # Same than 4.2.1.
hipster_selected = returning_hipster[random_hipster]
hipster_selected.records()

###########################################################
# 4.4. How many returning customers?
###########################################################
"""
The number of returning customer is just the sum of the length of the two lists (regular and hipster)
We also provide the number of hipster and the share they really represent in the total returning customers
"""

sim_returning = len(returning_regular) + len(returning_hipster)
sim_hipster = len(returning_hipster)
print("There are {} returning customers over the simulation.".format(str(sim_returning)))
print("Among which {} were hipsters! (with freq. {}) ".format(str(sim_hipster),
                                                              str(len(returning_hipster)/sim_returning)))
###########################################################
# 4.5. Do they have specific times when they show up more?
###########################################################

# In order to make things more readable, we just compute the frequencies hour by hour instead of
# minute by minute.
full_time = ["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00"]

# *********************************************************
# 4.5.1. For hipsters
# *********************************************************

# 4.5.1.1. Creation of a data frame that contains the frequency of appearance of hipsters
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

hipster_time = pd.DataFrame(full_time)  # A column with the different times
hipster_time[1] = [0 for i in range(10)]    # Another column that stores 0 (will be incremented)

# 4.5.1.2. Computing frequencies
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
The idea of section 4.5.1.2. is to look the history of all hipster and see when they purchased someting
We look at the time (hours to be more precise) and increment by 1 the corresponding "hour" in "hipster_time"
For example :
For HIP55 (fictive hipster)
Purchase Time
08:00
10:00
Then
hipster_time[1] = [1,0,1,0,...] = [1 observation for 8am, 0 for 9am, 1 for 10am...]
"""
for hipster in returning_hipster:                                                   # For every hipster in the list
    for observation in range(len(hipster.history)):                                 # For each observation in history
        for the_time in hipster_time[0]:                                            # For each hour in col 1
            the_time = str(the_time)[0:2]                                           # Take first 3 letters of the_time
            if hipster.history.iloc[observation, 1][0:2] == the_time:               # If the time coincides
                print(hipster.history.iloc[observation, 1][0:2], the_time, "True")  # Print what is on the left
                for i, element in enumerate(hipster_time[0]):                       # For every hour in hipster_time
                    element = str(element)[0:2]
                    if element == the_time:                                         # if the time coincides
                        hipster_time.iloc[i, 1] += 1                                # add 1 to the corresponding col
                    else:
                        print(element, the_time, "False")                           # In case of no time match

            else:
                print(hipster.history.iloc[observation, 1][0:2], the_time, "False")  # In case of no time match

# 4.5.1.3. Plotting frequencies
plt.bar(hipster_time[0], hipster_time[1])
plt.xlabel("Time")
plt.ylabel("Occurences")
plt.title("When do Hispter go to the coffee bar?")
plt.grid(True)
plt.savefig(os.path.abspath('./Results/Hispters_to_the_bar.png'))
plt.clf()

# *********************************************************
# 4.5.2. For Regular returning customers
# *********************************************************
"""
Exactly the same principles apply here than for 4.5.1. (hipsters)
"""

# 4.5.2.1. Creation of a data frame that contains the frequency of appearance of regular customers
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

regular_time = pd.DataFrame(full_time)
regular_time[1] = [0 for i in range(10)]

# 4.5.2.2. Computing frequencies
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

for hipster in returning_hipster:
    for observation in range(len(hipster.history)):
        for the_time in regular_time[0]:
            the_time = str(the_time)[0:2]
            if hipster.history.iloc[observation, 1][0:2] == the_time:
                print(hipster.history.iloc[observation, 1][0:2], the_time, "True")
                for i, element in enumerate(regular_time[0]):
                    element = str(element)[0:2]
                    if element == the_time:
                        regular_time.iloc[i, 1] += 1
                    else:
                        print(element, the_time, "False")

            else:
                print(hipster.history.iloc[observation, 1][0:2], the_time, "False")

# 4.5.2.3. Plotting frequencies
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

plt.bar(regular_time[0], regular_time[1])
plt.xlabel("Time")
plt.ylabel("Occurences")
plt.title("When do Regular customer (except Hispsters) go to the bar?.")
plt.grid(True)
plt.savefig(os.path.abspath('./Results/Regular_to_the_bar.png'))
plt.clf()

# *********************************************************
# 4.5.3. Both of them
# *********************************************************
"""
Same thing except we only add up frequencies (absolute numbers, not relative ones)
"""

# 4.5.3.1. Creation of a data frame that contains the frequency of appearance of all returning customers
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

global_frenquencies = pd.DataFrame(data=None, index=range(10))
global_frenquencies[0] = full_time
global_frenquencies[1] = hipster_time[1] + hipster_time[1]

# 4.5.3.2. Plotting global frequencies
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

plt.bar(global_frenquencies[0], global_frenquencies[1])
plt.xlabel("Time")
plt.ylabel("Occurences")
plt.title("When do returning customer go to the coffee bar?")
plt.grid(True)
plt.savefig(os.path.abspath('./Results/Returning_to_the_bar.png'))
plt.clf()

###########################################################
# 4.6. Can you determine a probability of having a onetime or returning customer at a given time?
###########################################################

# *********************************************************
# 4.6.1. Definition of a function that counts returning customers
# *********************************************************
"""
We subset "my_simulation" where "Full_Hour" = tempus ("time" in latin) and look at customer numbers.
If they start by either "RET" or "HIP", we add one to the counter.
At the end, we just compute the probability by dividing by total observations for given hour times 100.
"""


def count_returning_customer(tempus):
    tempus = tempus[0:2]
    subset = my_simulation[my_simulation["Full_Hour"] == tempus]
    n = len(subset)
    counter = 0
    for my_element in subset["Customer"]:
        my_element = my_element[0:3]
        if my_element == "RET" or my_element == "HIP":
            counter += 1
    print(int((counter/n)*100))
    return int((counter/n)*100)

# *********************************************************
# 4.6.2. Computing probabilities that a returning customer comes
# *********************************************************


# We parallelise the computations
prob_returner = Parallel(n_jobs=num_cores)(delayed(count_returning_customer)(i) for i in full_time)
print("Probabilities returning customers: ", prob_returner)

# *********************************************************
# 4.6.3. Computing probabilities that a OneTime customer enters
# *********************************************************
# The probability of a one-timer at a given time is 100% - prob of returner
prob_onetimer = [100 - prob_returner[i] for i in range(len(prob_returner))]
print("Probabilities of one-timer customers:", prob_onetimer)

# *********************************************************
# 4.6.4. Plotting
# *********************************************************

# 4.6.4.1. Plotting for one-timer customers
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

plt.bar(full_time, prob_onetimer)
plt.title("Probability of one-timer over day.")
plt.xlabel("Time")
plt.ylabel("Probability")
plt.savefig(os.path.abspath('./Results/prob_onetimer_per_hour.png'))
plt.clf()

# 4.6.4.2. Plotting for returning customers
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

plt.bar(full_time, prob_returner)
plt.title("Probability of returner over day.")
plt.xlabel("Time")
plt.ylabel("Probability")
plt.savefig(os.path.abspath('./Results/prob_returner_per_hour.png'))
plt.clf()

###########################################################
# 4.7. How does this impact their buying history?
###########################################################
"""
The idea behind section 4.7. is to have a look to the average time that a returning customer goes to coffee bar.
In other words, in order to know how many times a customer went to the bar is to simply have a look to the length
of his/her history. If the length is 3 => had been three times in the bar.
This measure is very helpful to understand how frequently do customer go the bar.
"""
# *********************************************************
# 4.7.1. For hipsters
# *********************************************************

hipster_history_size = []  # Empty lists that will contain how many time a customer had been to the bar
for my_client in returning_hipster:                      # For each customer in the list
    hipster_history_size.append(len(my_client.history))  # We add his/her amount of times he/she had been at the bar
hipster_sum_histories = sum(hipster_history_size)        # We sum all these observations
hipster_avg_history_size = int(hipster_sum_histories / len(hipster_history_size))   # And get the average
print("Hipster customers purchase {} time(s) on average from the coffee bar".format(hipster_avg_history_size))

# *********************************************************
# 4.7.2. For Regular returning customers
# *********************************************************

# Same principles than in 4.6.1.
regular_history_size = []
for my_client in returning_regular:
    regular_history_size.append(len(my_client.history))
regular_sum_histories = sum(regular_history_size)
regular_avg_history_size = int(regular_sum_histories / len(regular_history_size))
print("Regular returning customers purchase {} time(s) from average in the coffee bar".format(regular_avg_history_size))

###########################################################
# 4.8 Recap
###########################################################
print(regular_selected)
regular_selected.records()
print("Food items sold :", no_nothing_foods)
print("Amount of food items sold :", amount_food)
print("Drinks items sold :", drinks)
print("Amount of drinks item sold:", amount_drink)
print(hipster_selected)
hipster_selected.records()
print("Probabilities returning customers: ", prob_returner)
print("Probabilities of one-timer customers:", prob_onetimer)
print("Hipster customers purchase {} time(s) on average from the coffee bar".format(hipster_avg_history_size))
print("Regular returning customers purchase {} time(s) from average in the coffee bar".format(regular_avg_history_size))

# TIME ####################################################
total_time = time.time() - time_begin
minutes = int(total_time / 60)
seconds = int(((total_time / 60) - minutes) * 60)
print("Simulation carried out in {} minutes and {} seconds!".format(str(minutes), str(seconds)))
###########################################################
