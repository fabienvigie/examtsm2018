"""
Name : myClass
Authors : Fabien Vigié, Adriana De La Cruz
OS : Linux Ubuntu 14.04 LTS (works also fine on Macs)

The purpose of this script is to define the different classes used in our project.
These classes will be called on during the simulation process taking place in another script.

Organisation : The project classes are organised as follows : there is a super-class called
"Customer" with an ID and an initial budget of 0€ (overwritten later on). A Customer has several methods :
he/she can pay when he/she purchases food or drinks from the coffee bar.

"Customer" is subclassed by "OneTime" and "Returning" classes. The OneTime class corresponds to the class
of non-returning customers whereas the "Returning" class is made of returning customers. These two classes have their
specific ID (inherited from "Customer") and budget (overwrites "Customer"'s budget). However, the "Returning" class
has a specific attribute : a history record of purchases done in the past. Also, "Returning" has its own
methods such as "payh" (for "PAY with History") that is similar to the method defined in "Customer" but
with an history feature.

Finally, "OneTime" and "Returning" are each subclassed by "TripAdvisor" and "Hipster" respectively. These two
classes inherit from their parent-class of all attributes and methods (with different budgets). In the case of
"TripAdvisor" class, people can leave a tip (method "pay_tip").

Remark : We have consciously avoided using getters and setters given that all properties are public by definition
in Python. Besides, it avoids writing twice the same code with "set" and "get".

                                    Customer
                                        attributes :
                                        *id
                                        *budget
                                        methods :
                                        *pay()
                                        *discount()
        OneTime                                                         Returning
            attributes :                                                    attributes :
            *id                                                             *id
            *budget                                                         *budget
                                                                            *history
            methods :                                                       methods :
                                                                            *payh()
                                                                            *records()
                                                                            *profits()

        TripAdvisor                                                     Hipster
            attributes :                                                    attributes :
            *id                                                             *id
                                                                            *budget
            methods :                                                       methods :
            *pay_tip()

Contents;
2.1. Loading libraries
2.2. Writing the heads name of "history" in a compact variable
2.3. Creating classes
    2.3.1. "Customer" Super-class Customer (NEVER used directly in the project)
        2.3.1.1. "OneTime" class
            2.3.1.1.1. TripAdvisor class
        2.3.1.2. Returning class
            2.3.1.2.1. Hipster class
"""
###########################################################
# PART 2
###########################################################

###########################################################
# 2.1. Loading libraries
###########################################################

from random import randint
import pandas as pd

###########################################################
# 2.2. Writing the heads name of "history" in a compact variable
###########################################################

cols = ["Date", "Time", "Drink", "Food", "Total Paid", "Budget after"]

###########################################################
# 2.3. Creating classes
###########################################################


#   2.3.1. "Customer" Super-class Customer (NEVER used directly in the project)
class Customer(object):
    def __init__(self, identity):  # constructor special method
        self.id = identity         # gives an identity
        self.budget = 0            # set a default initial budget of 0

    def __repr__(self):            # special method telling python what to show if "self" is called via print()
        return "{} has a budget of {} €.".format(self.id, self.budget)

    def pay(self, dprice, fprice):              # allows customer to pay
        if self.budget - dprice - fprice < 0:   # verifying that they can afford to pay
            print("Payment refused!")           # if not, "payment refuse"
        else:                                   # otherwise
            self.budget -= dprice + fprice      # customer budget decreased by the amount of goods purchased

    def discount(self, price, discount):        # unused method for now
        self.budget += -(price*(1-discount))


#   2.3.1.1. "OneTime" class
class OneTime(Customer):                        # subclass of Customer, regular one-time customers
    def __init__(self, identity):               # constructor special method
        Customer.__init__(self, identity)       # calls parent-class constructor
        self.id = identity                      # allow OneTime to inherit of identity from Customer
        self.budget = 100                       # specific budget of 100


#   2.3.1.1.1. TripAdvisor class
class TripAdvisor(OneTime):                     # subclass of OneTime, TripAdvisor customers
    def __init__(self, identity):               # constructor special method
        OneTime.__init__(self, identity)        # call parent-class constructor
        self.id = identity                      # inherits of identity from OneTime (inheriting from Customer)

    def pay_tip(self):                          # methods allowing to pay tips
        tip = randint(1, 10)                    # a tip is a random number between 1 and 10
        self.budget -= tip                      # the budget is decreased by the amount of the tip
        print("{} paid a tip {} €.".format(self.id, tip))
        return tip                              # returns tip


#   2.3.1.2. Returning class
class Returning(Customer):                      # subclass of Customer, regular returning customers
    def __init__(self, identity):               # constructor special method
        Customer.__init__(self, identity)       # calls parent-class constructor
        self.id = identity                      # inherits of identity from Customer
        self.budget = 250                       # specific budget
        self.history = pd.DataFrame(data=None, index=None, columns=cols)  # allows to remember what had been purchased

    def payh(self, date, time, drink, dprice, food, fprice):    # pay with history method, make a recorded payment
        if self.budget - dprice - fprice < 0:                   # verifying that the customer has the budget required
            print("Payment refused!")                           # if not, "payment refused"
        else:                                                   # otherwise
            self.budget -= dprice + fprice                      # budget is decreased by the amount of goods purchased
            temp = pd.DataFrame([[date, time, drink, food, dprice+fprice, self.budget]], columns=cols)
            self.history = pd.DataFrame.append(self.history, temp, ignore_index=True)
            # we create a "temp" (for "temporary") data frame that receives several data
            # Then, "temp" is concatenated to self.history, the history grows

    def records(self):          # method to print the whole history
        print(self.history)

    def profits(self):          # method to print and return profits made
        print(self.history["Total Paid"])
        return self.history["Total Paid"]


#   2.3.1.2.1. Hipster class
class Hipster(Returning):                   # subclass of Returning, Hipster customers
    def __init__(self, identity):           # constructor special method
        Returning.__init__(self, identity)  # calls parent-class constructor
        self.id = identity                  # inherits of identity from Returning (inheriting from Customer)
        self.budget = 500                   # specific budget
