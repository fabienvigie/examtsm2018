from random import randint, sample

nbrReturning = 1000
shrHipster = 1/10
nbrHipster = int(nbrReturning * shrHipster)

if nbrReturning % shrHipster**(-1) != 0:
    nbrRegular = int(nbrReturning * (1 - shrHipster) + 1)
else:
    nbrRegular = int(nbrReturning * (1 - shrHipster))


# Faire une nouvelle méthode comme ci-dessous
sample = sample(range(10000000, 99999999), k=nbrReturning)
listingsRET = ["RET" + str(sample[i]) for i in range(nbrRegular)]
listingHIP = ["HIP" + str(sample[i]) for i in range(nbrRegular, nbrReturning)]
listings = listingsRET + listingHIP

print(listings)


